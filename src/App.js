import React from 'react';
import './App.css';
import { Layout } from 'antd';

import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import Contact from './contact/Contact';
import Profile from './profile/Profile';
import Bookmarks from './bookmarks/Bookmarks';
import Articles from './articles/Articles';
import SolidData from './SolidData';
import Menu from './menu/Menu';
import { useProfile } from './profile/useProfile';
import { Talks } from './talks/Talks';

const { Footer, Content, Header } = Layout;

const me =
    process.env.REACT_APP_WEB_ID || 'http://localhost:3333/profile/card#me';

function App() {
    const profile = useProfile(me);
    const { loading, error } = profile;
    return (
        <Router>
            <Layout style={{ minHeight: '100vh' }}>
                <Layout>
                    <Header>
                        <Menu webId={me} />
                    </Header>
                    <Content style={{ padding: '2rem' }}>
                        <Switch>
                            <Route exact path="/">
                                <SolidData />
                            </Route>
                            <Route path="/profile">
                                <Profile webId={me} {...profile} />
                            </Route>
                            <Route path="/bookmarks">
                                <Bookmarks webId={me} />
                            </Route>
                            <Route path="/talks">
                                <Talks webId={me} />
                            </Route>
                            <Route path="/contact">
                                <Contact webId={me} />
                            </Route>
                            <Route path="/articles">
                                <Articles webId={me} />
                            </Route>
                        </Switch>
                    </Content>
                    <Footer>
                        {loading || error ? (
                            <a href={me}>{me}</a>
                        ) : (
                            <>
                                <p>
                                    This is a public homepage of{' '}
                                    <strong>{profile.name}</strong>, whose WebID
                                    is <a href={me}>{me}</a>
                                </p>
                                <p>
                                    Like it?{' '}
                                    <span role="img" aria-label="Heart">
                                        ❤
                                    </span>
                                    ️{' '}
                                    <a href="https://gitlab.com/angelo-v/pod-homepage">
                                        Deploy it to your Pod!
                                    </a>
                                </p>
                            </>
                        )}
                    </Footer>
                </Layout>
            </Layout>
        </Router>
    );
}

export default App;
