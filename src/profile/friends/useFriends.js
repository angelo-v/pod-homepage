import data from '@solid/query-ldflex';
import { useEffect, useState } from 'react';
import { loadProfile } from '../loadProfile';

export const useFriends = webId => {
    const [friends, setFriends] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            for await (const uri of data[webId].friends) {
                loadProfile(uri).then(friend =>
                    setFriends(list => [...list, friend]),
                );
            }
        };
        fetchData();
    }, [webId, setFriends]);
    return friends;
};
