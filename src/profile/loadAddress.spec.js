import data from '@solid/query-ldflex';
import { loadAddress } from './loadAddress';
import { value } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));

describe('when loading address', () => {
    describe('from profile without an address', () => {
        let address;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {};
            address = await loadAddress('https://pod.example/jane#me');
        });

        it('the result is empty', () => {
            expect(address).toEqual({});
        });
    });

    describe('from profile with an empty address', () => {
        let address;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {
                vcard_hasAddress: value(
                    'https://pod.example/address#it'
                ),
            };
            data['https://pod.example/address#it'] = {};
            address = await loadAddress('https://pod.example/jane#me');
        });

        it('the result is empty', () => {
            expect(address).toEqual({});
        });
    });

    describe('from profile with an address', () => {
        let address;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {
                vcard_hasAddress: value(
                    'https://pod.example/address#it'
                ),
            };
            data['https://pod.example/address#it'] = {
                vcard_locality: value('Hamburg'),
                'vcard:country-name': value('Germany'),
            };
            address = await loadAddress('https://pod.example/jane#me');
        });

        describe('then the result contains', () => {
            it('the locality', () => {
                expect(address.locality).toEqual('Hamburg');
            });
            it('the country', () => {
                expect(address.country).toEqual('Germany');
            });
        });
    });
});
