import React from 'react';
import { Avatar, Card, Col, Layout, Row, Skeleton, Typography } from 'antd';
import Friends from './friends/Friends';
import { useFriends } from './friends/useFriends';
import { ProfilePicture } from './ProfilePicture';
import { Organization } from './Organization';
import { Location } from './Location';

const { Content } = Layout;
const { Title } = Typography;
const { Meta } = Card;

export const Error = () => (
    <Card className="example">
        <Avatar shape="square" icon="user" size="large" />
        <Meta title="Oops" description="Something went wrong." />
    </Card>
);

export default ({
    webId,
    loading,
    error,
    name,
    imageSrc,
    role,
    organization,
    country,
    locality,
}) => {
    if (error) return <Error />;
    const friends = useFriends(webId);
    return (
        <Layout>
            <Content>
                <Row className="profileHeader" type="flex" justify="center">
                    <Skeleton loading={loading} active avatar={{ size: 150 }}>
                        <Col sm={24} md={6}>
                            <ProfilePicture src={imageSrc} />
                        </Col>
                        <Col sm={24} md={10}>
                            <Title level={1}>{name}</Title>
                            <div className="info">
                                <div>{role}</div>
                                <div>
                                    <Organization name={organization} />
                                </div>
                                <div>
                                    <Location
                                        locality={locality}
                                        country={country}
                                    />
                                </div>
                            </div>
                        </Col>
                    </Skeleton>
                </Row>

                <Card loading={loading}>
                    <Row>
                        <Col sm={24} md={8}>
                            <Friends friends={friends} />
                        </Col>
                    </Row>
                </Card>
            </Content>
        </Layout>
    );
};
