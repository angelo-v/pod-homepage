import React from 'react';

import { shallow } from 'enzyme';
import { ProfilePicture } from './ProfilePicture';
import { Avatar } from 'antd';

describe('profile picture', () => {
    it('renders an avatar using the image src', () => {
        const result = shallow(
            <ProfilePicture src="https://pod.example/pic.png" />
        );
        let avatar = result.find(Avatar);
        expect(avatar).toHaveProp('src', 'https://pod.example/pic.png');
    });
    it('renders a user icon avatar when no src is given', () => {
        const result = shallow(<ProfilePicture />);
        expect(result.find(Avatar)).toHaveProp('icon', 'user');
    });
});
