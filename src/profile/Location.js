import { Icon } from 'antd';
import React from 'react';

export const Location = ({ locality, country }) => {
    if (!country && !locality) return null;
    return (
        <>
            <Icon type="environment" /> {printLocation(locality, country)}
        </>
    );
};

function printLocation(locality, country) {
    if (locality && country) {
        return `${locality}, ${country}`;
    }
    return locality || country;
}
