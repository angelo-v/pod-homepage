import React from 'react';
import { useTalks } from './useTalks';
import { Card, Icon, List, Typography } from 'antd';
import { Resources } from './Resources';

const { Paragraph } = Typography;
const { Meta } = Card;

const Talk = ({ uri, title, date, description, slides, recordings }) => (
    <Card
        style={{ margin: '1rem', maxWidth: 800 }}
        title={
            <>
                {title}{' '}
                <a href={uri}>
                    <Icon type="link" />
                </a>
            </>
        }
        extra={
            <>
                <Icon type="calendar" key="date" /> {date.toLocaleDateString()}
            </>
        }
    >
        <Meta
            description={
                <Paragraph ellipsis={{ rows: 1, expandable: true }}>
                    {description}
                </Paragraph>
            }
        />
        <Resources talkTitle={title} slides={slides} recordings={recordings} />
    </Card>
);

export const Talks = ({ webId }) => {
    const { talks, loading } = useTalks(webId);
    return (
        <List
            header="Talks"
            itemLayout="horizontal"
            loading={loading}
            dataSource={talks}
            renderItem={item => <Talk {...item} />}
        />
    );
};
