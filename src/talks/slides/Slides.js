import React from 'react';
import { MimeTypeIcon } from './MimeTypeIcon';

export const Slides = ({ list }) => {
    return list.map(file => (
        <p key={file.uri}>
            <a href={file.contentUrl || file.uri}><MimeTypeIcon mimeType={file.mimeType}/> {file.name}</a>
        </p>
    ));
};
