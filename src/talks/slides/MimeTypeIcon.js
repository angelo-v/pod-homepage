import { Icon } from 'antd';
import React from 'react';

const types = {
    'application/pdf': 'file-pdf',
    'text/html': 'html5',
};

export const MimeTypeIcon = ({ mimeType }) => {
    const type = types[mimeType] || 'file';
    return <Icon type={type} />;
};
