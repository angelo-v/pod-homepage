import data from '@solid/query-ldflex';
import { loadMedia } from '../media/loadMedia';

export  const loadSlides = async  (talkUri) => {
    const result = [];

    const slides = await data[talkUri].schema_workFeatured;
    if (!slides) return result;

    for await (const uri of data[slides].schema_associatedMedia) {
        const media = await loadMedia(uri.toString());
        result.push(media);
    }
    return result;
}