import data from '@solid/query-ldflex';
import { emptyList, list, value } from '../test-utils/ldflex';
import { loadTalks } from './loadTalks';
import { loadTalk } from './loadTalk';

jest.mock('@solid/query-ldflex', () => ({}));
jest.mock('./loadTalk');

describe('when loading talks', () => {
    describe('from a person that did none', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example'] = {
                schema_performerIn: emptyList(),
            };
            result = await loadTalks('https://person.example');
        });

        it('the result is an empty array', () => {
            expect(result).toEqual([]);
        });
    });

    describe('from a person that did one talk', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example'] = {
                schema_performerIn: list(['https://person.example/talk#it']),
            };
            data['https://person.example/talk#it'] = {
                schema_name: value('Some talk'),
                schema_description: value('A longer description of the talk'),
                schema_startDate: value('2019-05-19'),
            };
            result = await loadTalks('https://person.example');
        });

        it('the result contains 1 talk', () => {
            expect(result).toHaveLength(1);
        });

        it('data of that talk was loaded', () => {
            expect(loadTalk).toHaveBeenCalledWith(
                'https://person.example/talk#it'
            );
        });
    });

    describe('from a person that did three talks', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example'] = {
                schema_performerIn: list([
                    'https://person.example/talk#1',
                    'https://person.example/talk#2',
                    'https://person.example/talk#3',
                ]),
            };
            loadTalk.mockResolvedValueOnce({
                uri: 'https://person.example/talk#1',
                date: new Date('2019-04-03'),
            });
            loadTalk.mockResolvedValueOnce({
                uri: 'https://person.example/talk#2',
            });
            loadTalk.mockResolvedValueOnce({
                uri: 'https://person.example/talk#3',
                date: new Date('2019-05-17'),
            });
            result = await loadTalks('https://person.example');
        });

        it('the result contains 3 talks', () => {
            expect(result).toHaveLength(3);
        });

        it('the talks are ordered by start date (new to old, unknown at the end)', () => {
            expect(result[0].uri).toBe('https://person.example/talk#3');
            expect(result[1].uri).toBe('https://person.example/talk#1');
            expect(result[2].uri).toBe('https://person.example/talk#2');
        });
    });

    describe('and each talk takes 300 ms to load', () => {
        const talkLoadingTime = 300;
        const acceptedTotalLoadingTime = 400;

        let allTalks;
        beforeEach(async () => {
            data['https://person.example'] = {
                schema_performerIn: list([
                    'https://person.example/talk#1',
                    'https://person.example/talk#2',
                    'https://person.example/talk#3',
                ]),
            };
            loadTalk.mockImplementation(() => {
                return new Promise(resolve => {
                    setTimeout(() => resolve({}), talkLoadingTime);
                });
            });
            allTalks = await loadTalks('https://person.example');
        }, acceptedTotalLoadingTime);

        it('it should take less than 400 ms to load all those talks', () => {
            expect(allTalks).toHaveLength(3);
        });
    });
});
