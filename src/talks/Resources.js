import { Descriptions } from 'antd';
import React from 'react';
import { Slides } from './slides/Slides';
import {Recordings} from './recordings/Recordings';

export const Resources = ({ talkTitle, slides, recordings }) => {
    if (slides.length === 0 && recordings.length === 0) return null;
    return (
        <Descriptions bordered layout="vertical" size="small">
            {slides.length > 0 && (
                <Descriptions.Item label="Slides">
                    <Slides list={slides} />
                </Descriptions.Item>
            )}
            {recordings.length > 0 && (
                <Descriptions.Item label="Recordings">
                    <Recordings talkTitle={talkTitle} list={recordings} />
                </Descriptions.Item>
            )}
        </Descriptions>
    );
};
