import data from '@solid/query-ldflex';
import { loadTalk } from './loadTalk';
import { loadSlides } from './slides/loadSlides';
import { value } from '../test-utils/ldflex';
import { loadRecordings } from './recordings/loadRecordings';

jest.mock('@solid/query-ldflex', () => ({}));
jest.mock('./slides/loadSlides');
jest.mock('./recordings/loadRecordings');

describe('when loading a talk', () => {
    describe('without any data', () => {
        let talk;
        beforeEach(async () => {
            data['https://person.example/talks#1'] = {};
            talk = await loadTalk('https://person.example/talks#1');
        });

        it('result contains a uri', () => {
            expect(talk.uri).toBe('https://person.example/talks#1');
        });
        it('and uses uri as fallback title', () => {
            expect(talk.title).toBe('https://person.example/talks#1');
        });

        it('and does not have a description', () => {
            expect(talk.description).toBeUndefined();
        });

        it('and does not have a date', () => {
            expect(talk.date).toBeUndefined();
        });
    });

    describe('with basic data but no slides and recordings', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example/talk#it'] = {
                schema_name: value('Some talk'),
                schema_description: value('A longer description of the talk'),
                schema_startDate: value('2019-05-19'),
            };
            result = await loadTalk('https://person.example/talk#it');
        });

        it('the talk contains a title', () => {
            expect(result.title).toBe('Some talk');
        });

        it('and it contains the description', () => {
            expect(result.description).toBe('A longer description of the talk');
        });

        it('and it contains the date', () => {
            expect(result.date).toEqual(new Date('2019-05-19'));
        });

        it('and it contains the uri', () => {
            expect(result.uri).toBe('https://person.example/talk#it');
        });
    });

    describe('with slides', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example/talk#it'] = {};
            loadSlides.mockResolvedValue(['slides1', 'slides2']);
            result = await loadTalk('https://person.example/talk#it');
        });

        it('should load slides for the correct talk', () => {
            expect(loadSlides).toHaveBeenCalledWith(
                'https://person.example/talk#it'
            );
        });

        it('should contain all slides', () => {
            expect(result.slides).toEqual(['slides1', 'slides2']);
        });
    });

    describe('with recordings', () => {
        let result;
        beforeEach(async () => {
            data['https://person.example/talk#it'] = {};
            loadRecordings.mockResolvedValue(['rec1', 'rec2']);
            result = await loadTalk('https://person.example/talk#it');
        });

        it('should load recordings for the correct talk', () => {
            expect(loadRecordings).toHaveBeenCalledWith(
                'https://person.example/talk#it'
            );
        });

        it('should contain all recordings', () => {
            expect(result.recordings).toEqual(['rec1', 'rec2']);
        });
    });
});
