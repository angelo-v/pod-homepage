import React from 'react';
import { PlayVideo } from './PlayVideo';
import { Download } from './Download';
import { Button } from 'antd';

const ButtonGroup = Button.Group;

export const Recording = recording => {
    return (
        <ButtonGroup>
            <PlayVideo {...recording} />
            {recording.contentUrl ? (
                <Download name={recording.name} url={recording.contentUrl} />
            ) : null}
        </ButtonGroup>
    );
};
