import data from '@solid/query-ldflex';
import { loadMedia } from '../media/loadMedia';

export const loadRecordings = async (talkUri) => {
    const result = [];

    for await (const uri of data[talkUri].schema_recordedIn) {
        const media = await loadMedia(uri.toString());
        result.push(media);
    }
    return result;
};