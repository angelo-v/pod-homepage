import React from 'react';

export const Video = ({ name, contentUrl, embedUrl, mimeType }) =>
    contentUrl ? (
        <Html5Video src={contentUrl} mimeType={mimeType} filename={name} />
    ) : (
        <EmbeddedVideo src={embedUrl} title={name} />
    );

function Html5Video({ src, mimeType, filename }) {
    return (
        <video width="100%" controls autoPlay>
            <source src={src} type={mimeType} />
            Your browser does not support HTML5 video.{' '}
            <a href={src} download={filename}>
                Download video file.
            </a>
        </video>
    );
}

function EmbeddedVideo({ src, title }) {
    return (
        <iframe
            title={title}
            style={{
                width: '100%',
            }}
            src={src}
            width="640"
            height="363"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen
        />
    );
}
