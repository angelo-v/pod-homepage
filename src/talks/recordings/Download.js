import { Button } from 'antd';
import React from 'react';

export const Download = ({name, url}) => (
    <Button
        type="default"
        icon="download"
        download={name}
        href={url}
    >
        Download
    </Button>
);
