import { useAsync } from 'react-use';
import { loadTalks } from './loadTalks';

export const useTalks = webId => {
    const { value, loading, error } = useAsync(() => loadTalks(webId), [webId]);
    return { loading, error, talks: value || []};
};
