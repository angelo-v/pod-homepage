import data from '@solid/query-ldflex';
import { loadTalk } from './loadTalk';

export const loadTalks = async webId => {
    const pendingTalks = [];
    for await (const uri of data[webId].schema_performerIn) {
        pendingTalks.push(loadTalk(uri.toString()));
    }

    const talks = await Promise.all(pendingTalks);

    return talks.sort((a, b) => (b.date || 0) - (a.date || 0));
};
