import data from '@solid/query-ldflex';

export const loadMedia = async uri => {
    const name = await data[uri].schema_name;
    const description = await data[uri].schema_description;
    const mimeType = await data[uri].schema_encodingFormat;
    const contentUrl = await data[uri].schema_contentUrl;
    const embedUrl = await data[uri].schema_embedUrl;
    const fallbackName = contentUrl ? contentUrl.toString() : uri;
    return {
        uri,
        name: name ? name.toString() : fallbackName,
        description: description && description.toString(),
        mimeType: mimeType && mimeType.toString(),
        contentUrl: contentUrl && contentUrl.toString(),
        embedUrl: embedUrl && embedUrl.toString()
    };
};