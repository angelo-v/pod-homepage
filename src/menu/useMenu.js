import data from '@solid/query-ldflex';
import { useAsync } from 'react-use';

export const useMenu =  (webId) => {
    const talks = useAsync(() => data[webId].schema_performerIn, [webId]);

    return {
        talks: talks.value
    }
};