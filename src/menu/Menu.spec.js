import React from 'react';
import { useMenu } from './useMenu';
import { shallow } from 'enzyme';
import { Link } from 'react-router-dom';

import Menu from './Menu';

jest.mock('./useMenu');

describe('Menu', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe('talks', () => {
        it('is present', () => {
            useMenu.mockReturnValue({ talks: true });
            const menu = shallow(<Menu webId={'https://person.example/#me'} />);
            expect(menu.find(Link).filter('[to="/talks"]')).toExist();
        });
        it('is missing', () => {
            useMenu.mockReturnValue({ talks: false });
            const menu = shallow(<Menu webId={'https://person.example/#me'} />);
            expect(menu.find(Link).filter('[to="/talks"]')).not.toExist();
        });
    });
});
