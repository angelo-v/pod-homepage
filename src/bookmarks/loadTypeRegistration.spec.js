import data from '@solid/query-ldflex';
import { loadTypeRegistration } from './loadTypeRegistration';
import { list, value } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));

describe('load type registration', () => {
    it('returns the URI of the instance registered for the type', async () => {
        data['https://pod.example/type-index'] = {
            dct_references: list(['https://pod.example/type-index#1']),
        };
        data['https://pod.example/type-index#1'] = {
            solid_instance: value('https://pod.example/bookmarks'),
            solid_forClass: value('https://vocab.example/Bookmark'),
        };
        const result = await loadTypeRegistration(
            'https://pod.example/type-index',
            'https://vocab.example/Bookmark'
        );
        expect(result).toBe('https://pod.example/bookmarks');
    });

    it('returns undefined, if no registrations are present', async () => {
        data['https://pod.example/type-index'] = {
            dct_references: list([]),
        };
        const result = await loadTypeRegistration(
            'https://pod.example/type-index',
            'https://vocab.example/Bookmark'
        );
        expect(result).toBeUndefined();
    });

    it('returns undefined, if registration misses a forClass', async () => {
        data['https://pod.example/type-index'] = {
            dct_references: list(['https://pod.example/type-index#1']),
        };
        data['https://pod.example/type-index#1'] = {
            solid_instance: value('https://pod.example/bookmarks'),
        };
        const result = await loadTypeRegistration(
            'https://pod.example/type-index',
            'https://vocab.example/Bookmark'
        );
        expect(result).toBeUndefined();
    });

    it('returns undefined if registration has no instance', async () => {
        data['https://pod.example/type-index'] = {
            dct_references: list(['https://pod.example/type-index#1']),
        };
        data['https://pod.example/type-index#1'] = {
            solid_forClass: value('https://vocab.example/Bookmark'),
        };
        const result = await loadTypeRegistration(
            'https://pod.example/type-index',
            'https://vocab.example/Bookmark'
        );
        expect(result).toBeUndefined();
    });

    it('returns undefined, if no registrations for the class are present', async () => {
        data['https://pod.example/type-index'] = {
            dct_references: list(['https://pod.example/type-index#1']),
        };
        data['https://pod.example/type-index#1'] = {
            solid_instance: value('https://pod.example/something'),
            solid_forClass: value('https://vocab.example/Something'),
        };
        const result = await loadTypeRegistration(
            'https://pod.example/type-index',
            'https://vocab.example/Bookmark'
        );
        expect(result).toBeUndefined();
    });
});
