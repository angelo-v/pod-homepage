import data from '@solid/query-ldflex';
import loadBlogPosts from './loadBlogPosts';
import loadPost from './loadPost';
import { emptyList, value, list } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));
jest.mock('./loadPost');

describe('when loading a blog', () => {
    describe('successfully with data but no posts', () => {
        let allPosts;
        beforeEach(async () => {
            data['https://blog.example#it'] = {
                label: value('My cool blog'),
                foaf_homepage: value('https://blog.example/'),
                sioc_container_of: emptyList(),
            };
            allPosts = await loadBlogPosts('https://blog.example#it');
        });

        it('the result is empty', () => {
            expect(allPosts).toEqual([]);
        });
    });

    describe('successfully with data and 3 posts', () => {
        let allPosts;
        beforeEach(async () => {
            data['https://blog.example#it'] = {
                label: value('My cool blog'),
                foaf_homepage: value('https://blog.example/'),
                sioc_container_of: list([
                    'https://blog.example/posts/1',
                    'https://blog.example/posts/2',
                    'https://blog.example/posts/3',
                ]),
            };
            loadPost
                .mockResolvedValueOnce({
                    uri: 'https://blog.example/posts/1',
                    title: 'post 1',
                })
                .mockResolvedValueOnce({
                    uri: 'https://blog.example/posts/2',
                    title: 'post 2',
                })
                .mockResolvedValueOnce({
                    uri: 'https://blog.example/posts/3',
                    title: 'post 3',
                });
            allPosts = await loadBlogPosts('https://blog.example#it');
        });

        it('the result contains 3 posts', () => {
            expect(allPosts).toHaveLength(3);
        });

        it('contains the loaded post data', () => {
            expect(allPosts).toEqual([
                {
                    uri: 'https://blog.example/posts/1',
                    title: 'post 1',
                    blog: {
                        name: 'My cool blog',
                        homepage: 'https://blog.example/',
                    },
                },
                {
                    uri: 'https://blog.example/posts/2',
                    title: 'post 2',
                    blog: {
                        name: 'My cool blog',
                        homepage: 'https://blog.example/',
                    },
                },
                {
                    uri: 'https://blog.example/posts/3',
                    title: 'post 3',
                    blog: {
                        name: 'My cool blog',
                        homepage: 'https://blog.example/',
                    },
                },
            ]);
        });
    });

    describe('and each post takes 300 ms to load', () => {
        const postLoadingTime = 300;
        const acceptedTotalLoadingTime = 400;


        let allPosts;
        beforeEach(async () => {
            data['https://blog.example#it'] = {
                sioc_container_of: list(['1','2','3'])
            };
            loadPost.mockImplementation(() => {
                return new Promise(resolve => {
                    setTimeout(() => resolve({}), postLoadingTime);
                });
            });
            allPosts = await loadBlogPosts('https://blog.example#it');
        }, acceptedTotalLoadingTime);

        it('it should take less than 400 ms to load all those posts', () => {
            expect(allPosts).toHaveLength(3);
        });
    });
});
