import { timeout } from 'promise-timeout';

import data from '@solid/query-ldflex';

export default async (uri, timeoutMillis = 5000) => {
    try {
        const title = await timeout(data[uri].dct_title, timeoutMillis);
        const created = await timeout(data[uri].dct_created, timeoutMillis);
        return {
            uri: uri,
            title: title ? title.toString() : uri,
            created: created ? new Date(created.toString()) : null,
        };
    } catch (e) {
        console.warn(
            `Failed to load post ${uri} within timeout of ${timeoutMillis} ms`
        );
        return {
            uri,
            title: uri,
            created: null,
        };
    }
};
