import loadPost from './loadPost';
import data from '@solid/query-ldflex';
import { value } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));

describe('when loading a post', () => {
    describe('successfully with all data available', () => {
        let post;
        beforeEach(async () => {
            data['https://blog.example/post/1'] = {
                dct_title: value('10 awesome things'),
                dct_created: value('2010-02-12'),
            };
            post = await loadPost('https://blog.example/post/1');
        });

        it('the result contains the uri', () => {
            expect(post.uri).toBe('https://blog.example/post/1');
        });

        it('the result contains the title', () => {
            expect(post.title).toBe('10 awesome things');
        });

        it('the result contains the creation date', () => {
            expect(post.created).toEqual(new Date('2010-02-12'));
        });
    });

    describe('successfully, but with missing data', () => {
        let post;
        beforeEach(async () => {
            data['https://blog.example/post/1'] = {
                dct_title: Promise.resolve(undefined),
                dct_created: Promise.resolve(undefined),
            };
            post = await loadPost('https://blog.example/post/1');
        });

        it('the result contains the uri', () => {
            expect(post.uri).toBe('https://blog.example/post/1');
        });

        it('the result contains the uri as a fallback title', () => {
            expect(post.title).toBe('https://blog.example/post/1');
        });

        it('the result does not contain the creation date', () => {
            expect(post.created).toBeNull();
        });
    });

    describe('is rejected while accessing the dct_title', () => {
        let post;
        beforeEach(async () => {
            data['https://blog.example/post/1'] = {
                dct_title: Promise.reject(),
            };
            post = await loadPost('https://blog.example/post/1');
        });

        it('the result contains the uri', () => {
            expect(post.uri).toBe('https://blog.example/post/1');
        });

        it('the result contains the uri as a fallback title', () => {
            expect(post.title).toBe('https://blog.example/post/1');
        });

        it('the result does not contain the creation date', () => {
            expect(post.created).toBeNull();
        });
    });

    describe('remains pending, e.g. due to CORS issues', () => {
        let post;
        beforeEach(async () => {
            data['https://blog.example/post/1'] = {
                dct_title: new Promise(() => 'pending forever'),
            };
            post = await loadPost('https://blog.example/post/1', 10);
        });

        it('the result contains the uri', () => {
            expect(post.uri).toBe('https://blog.example/post/1');
        });

        it('the result contains the uri as a fallback title', () => {
            expect(post.title).toBe('https://blog.example/post/1');
        });

        it('the result does not contain the creation date', () => {
            expect(post.created).toBeNull();
        });
    });
});
