import { loadAccounts } from './loadAccounts';

import { useAsync } from 'react-use';

export const useAccounts = webId => {
    const { value, loading, error } = useAsync(() => loadAccounts(webId), [
        webId,
    ]);
    return {
        accounts: value || [],
        loading,
        error,
    };
};
