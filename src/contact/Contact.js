import React from 'react';
import { Accounts } from './Accounts';

export default ({ webId }) => <Accounts webId={webId} />;
