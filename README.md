# Pod Homepage for Solid

A public homepage based on data in your [Solid Pod](https://solidproject.org).

 * Load personal data from your Pod to display a nice homepage
 * Host it on your Solid Pod or elsewhere
 
See my homepage as an example: https://angelo.veltens.org
 
## Features

 * Personal profile page
 * Social Bookmarks
 * Blog articles
 * Contact data
 * Talks including slides and recordings
 
## Deploy to your Pod

tl;dr:
 1. `git clone https://gitlab.com/angelo-v/pod-homepage.git`
 2. Adjust `.env` file
 3. `npm run deploy`
 
### 1. Checkout this repo

```bash
git clone https://gitlab.com/angelo-v/pod-homepage.git
```

### 2. Adjust then `.env` file:

Open the file `.env` in the root of this repo and adjust the variables:

`PUBLIC_URL` is the URL you want to deploy to. This could be your Pod root,
but also any subdirectory within your Pod. 

`REACT_APP_WEB_ID` is the Web ID you want to generate the homepage for.

`SOLID_IDP` has to contain the URL of your Identity Provider
(e.g. https://solid.community).

`SOLID_USERNAME` is the username you are using to log in to your Pod.

The password does not need to be configured. It will be prompted.

**Example**: If you have an account `jdoe` at https://solid.community and want to replace
the default home page of your Pod, the `.env` file should look like this:

```.env
# Deployment
PUBLIC_URL=https://jdoe.solid.community/
REACT_APP_WEB_ID=https://jdoe.solid.community/profile/card#me

# Authentication
SOLID_IDP=https://solid.community
SOLID_USERNAME=jdoe

# Build
GENERATE_SOURCEMAP=false
```

### 3. Publish

Run  `npm run deploy` to start the build process and upload your homepage
to the Pod. You will be prompted for your password.